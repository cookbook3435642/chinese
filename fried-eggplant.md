# Fried Eggplant 炒茄子

source: [xiachufang](https://www.xiachufang.com/recipe/102759553/)

### Ingredients

| 食材  | Ingredients | Quantity|
| --- |:---|---:|
| 茄子  | Eggplant | 700g|
| 五花肉* | Pork belly* | 200g|
| 大蒜 | Garlic| 4 cloves|
| 葱 | Green onions | 2|
| 青椒 | Green bell/chili peppers | 2|
| 红辣椒 | Red chili pepper | 3|
| 姜 | ginger | 2g (1 teaspoon)| 
| 耗油 | Oyster sauce | 2 tablespoon|
| 美味鲜 | Tasty Fresh / soy sauce | 3 tablespoon|
| 蒸鱼豉油* | Steamed fish soy sauce* | 1 tablespoon|
| 油 | Oil | 4 tablespoon|

*: can be ignored

### Recipe

1. clean & cut
   - Eggplant --> small lang cuboid (approx. 1.5cm * 1.5cm * 7cm)
   - Pork belly --> small pieces (approx. 0.5cm * 0.5cm * 0.5cm)
   - Garlic --> small slices
   - Green Onions --> separately cut white and green parts into small rings
   - Green & Red chili pepper --> small pieces
   - Ginger --> small pieces
   
2. steam eggplant 18 min
3. heat the oil in a pan and fry the pork belly until it turns brown
4. add the following ingredients into the pan, fry for 2 min
   1. garlic 
   2. white part of green onion rings 
   3. green and red chili peppers 
   4. ginger
5. mix the following sauces into the pan
   1. oyster sauce
   2. tasty fresh
   3. steamed fish soy sauce
6. add steamed eggplant into the pan and fry for 1 min
7. **[important]** mash the eggplant in the pan, mix well with the other ingredients, and fry for 1 min
8. add salt and pepper according to personal preference, add green onion rings as topping
9. done

